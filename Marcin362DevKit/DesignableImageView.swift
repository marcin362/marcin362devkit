//
//  DesignableImageView.swift
//  Marcin362DevKit
//
//  Created by Marcin Ambrochowicz on 31.10.2016.
//  Copyright © 2016 Marcin Ambrochowicz. All rights reserved.
//

import UIKit

@IBDesignable public class DesignableImageView : UIImageView {

    // MARK: - IB
    
    @IBInspectable public var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable public var imageColor: UIColor? {
        didSet {
            if let color = imageColor {
                tintColor = color
                image = image?.withRenderingMode(.alwaysTemplate)
            }
        }
    }
    
}
