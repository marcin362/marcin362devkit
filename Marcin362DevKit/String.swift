//
//  String.swift
//  Marcin362DevKit
//
//  Created by Marcin Ambrochowicz on 01.12.2016.
//  Copyright © 2016 Marcin Ambrochowicz. All rights reserved.
//

import Foundation

extension String {
    
    public func isAlphanumeric() -> Bool {
        let characterSet = CharacterSet.alphanumerics.inverted
        
        let range = self.rangeOfCharacter(from: characterSet)
        
        return range?.lowerBound == range?.upperBound
    }
    
}
