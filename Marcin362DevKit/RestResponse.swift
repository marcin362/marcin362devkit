//
//  RestResponse.swift
//  Marcin362DevKit
//
//  Created by Marcin Ambrochowicz on 16.11.2016.
//  Copyright © 2016 Marcin Ambrochowicz. All rights reserved.
//

import Foundation

open class RestResponse {
    
    public enum Code : Int {
        case noInternet = -1009
        case undefined = 0
        case ok = 200
    }
    
    public let code: Code
    public let data: Data?
    public let string: String?
    public let json: [String : Any]?
    
    required public init(code: Int, data: Data?) {
        self.code = Code(rawValue: code) ?? .undefined
        
        self.data = data
        
        if let data = data {
            self.string = String(data: data,
                                 encoding: String.Encoding.utf8)
            
            self.json = (try? JSONSerialization.jsonObject(with: data,
                                                           options: JSONSerialization.ReadingOptions.allowFragments)) as? [String : Any]
        } else {
            self.string = nil
            self.json = nil
        }
    }
    
}
