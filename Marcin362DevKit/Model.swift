//
//  Model.swift
//  Marcin362DevKit
//
//  Created by Marcin Ambrochowicz on 12.11.2016.
//  Copyright © 2016 Marcin Ambrochowicz. All rights reserved.
//

import CoreData

public enum ModelError : Error {
    case persistentContainer(error: Error)
    case saveContext(error: Error)
}

public class Model {
    
    public init(name: String) throws {
        var persistentContainerError: Error?
        
        persistentContainer = NSPersistentContainer(name: name)
        persistentContainer.loadPersistentStores { (persistentStoreDescription, error) in
            persistentContainerError = error
        }
        
        guard persistentContainerError == nil else {
            throw ModelError.persistentContainer(error: persistentContainerError!)
        }
    }
    
    public func saveContext() throws {
        let context = persistentContainer.viewContext
        
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                throw ModelError.saveContext(error: error)
            }
        }
    }
    
    public func cancelContext() {
        let context = persistentContainer.viewContext
        
        context.rollback()
    }
    
    public func newManagedObject<T: NSManagedObject>(type: T.Type) -> T {
        let entityName = NSStringFromClass(type)
        
        let managedObject: T = NSEntityDescription.insertNewObject(forEntityName: entityName, into: persistentContainer.viewContext) as! T
        
        return managedObject
    }
    
    public func delete(managedObject: NSManagedObject) {
        let context = persistentContainer.viewContext
        
        context.delete(managedObject)
    }
    
    public func findManagedObjects<T: NSManagedObject>(type: T.Type, field: String, value: String) -> [T]? {
        let entityName = NSStringFromClass(type)
        
        let fetchRequest = NSFetchRequest<T>(entityName: entityName)
        fetchRequest.predicate = NSPredicate(format: "%K == %@", field, value)
        
        if let results = try? persistentContainer.viewContext.fetch(fetchRequest) {
            return results
        }
        
        return nil
    }
    
    public func fetchedResultsController<T: NSFetchRequestResult>(fetchRequest: NSFetchRequest<T>, sectionNameKeyPath: String? = nil, cacheName: String? = nil) -> NSFetchedResultsController<T> {
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: persistentContainer.viewContext,
                                                                  sectionNameKeyPath: sectionNameKeyPath,
                                                                  cacheName: cacheName)
        
        return fetchedResultsController
    }
    
    // MARK: - Private
    
    private let persistentContainer: NSPersistentContainer
    
}
