//
//  DesignableTextField.swift
//  Marcin362DevKit
//
//  Created by Marcin Ambrochowicz on 15.11.2016.
//  Copyright © 2016 Marcin Ambrochowicz. All rights reserved.
//

import UIKit

@IBDesignable public class DesignableTextField : UITextField {
    
    // MARK: - IB
    
    @IBInspectable public var placeholderColor: UIColor? {
        didSet {
            if placeholderColor != nil {
                attributedPlaceholder = NSAttributedString(string: placeholder ?? "",
                                                           attributes: [NSForegroundColorAttributeName: placeholderColor!])
            }
        }
    }
    
}
