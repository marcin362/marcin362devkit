//
//  UIViewController.swift
//  Marcin362DevKit
//
//  Created by Marcin Ambrochowicz on 19.11.2016.
//  Copyright © 2016 Marcin Ambrochowicz. All rights reserved.
//

import UIKit

extension UIViewController {
    
    public func showAlert(withMessage message: String) {
        let action = UIAlertAction(title: NSLocalizedString("UIViewController.showAlert.action.title", value: "OK", comment: "Default action title"),
                                   style: .default,
                                   handler: nil)
        
        let alert = UIAlertController(title: NSLocalizedString("UIViewController.showAlert.alert.title", value: "Message", comment: "Default alert title"),
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(action)
        
        present(alert, animated: true, completion: nil)
    }
    
}
