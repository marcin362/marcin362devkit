//
//  NotificationName.swift
//  Marcin362DevKit
//
//  Created by Marcin Ambrochowicz on 02.12.2016.
//  Copyright © 2016 Marcin Ambrochowicz. All rights reserved.
//

import Foundation

extension Notification.Name {
    
    static let defaultName = Notification.Name(rawValue: "DefaultName")
    
}
