//
//  UIColor.swift
//  Marcin362DevKit
//
//  Created by Marcin Ambrochowicz on 13.11.2016.
//  Copyright © 2016 Marcin Ambrochowicz. All rights reserved.
//

import UIKit

extension UIColor {
    
    public convenience init?(hex: String) {
        guard hex.hasPrefix("#") else {
            return nil
        }
        
        let start = hex.index(hex.startIndex, offsetBy: 1)
        let hexColor = hex.substring(from: start)

        guard hexColor.characters.count == 6 else {
            return nil
        }
        
        let scanner = Scanner(string: hexColor)
        
        var hexNumber: UInt64 = 0
        
        guard scanner.scanHexInt64(&hexNumber) else {
            return nil
        }
        
        let r = CGFloat((hexNumber & 0xff0000) >> 16) / 255
        let g = CGFloat((hexNumber & 0xff00) >> 8) / 255
        let b = CGFloat((hexNumber & 0xff) >> 0) / 255

        self.init(red: r, green: g, blue: b, alpha: 1.0)
    }
    
}
