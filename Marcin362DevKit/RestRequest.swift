//
//  RestRequest.swift
//  Marcin362DevKit
//
//  Created by Marcin Ambrochowicz on 16.11.2016.
//  Copyright © 2016 Marcin Ambrochowicz. All rights reserved.
//

import Foundation

open class RestRequest {
    
    public enum Method : String {
        case get = "GET"
        case post = "POST"
        case put = "PUT"
        case delete = "DELETE"
    }
    
    public let endpoint: String
    
    public var method: Method = .get
    public var params: [String : Any]? = nil
    public var bodyParams: [String : Any]? = nil
    public var headers: [String : String]? = nil
    
    public var paramsString: String? {
        return paramsString(fromParams: params)
    }
    
    public var bodyParamsString: String? {
        return paramsString(fromParams: bodyParams)
    }
    
    public init(endpoint: String) {
        self.endpoint = endpoint
    }
    
    // MARK: - Private
    
    private func paramsString(fromParams params: [String : Any]?) -> String? {
        guard let params = params else {
            return nil
        }
        
        var paramsString = [String]()
        for (key, value) in params {
            switch value {
            case let value as String:
                paramsString.append(String(format: "%@=%@", key, value))
            default:
                ()
            }
        }
        
        return paramsString.joined(separator: "&")
    }
    
}
