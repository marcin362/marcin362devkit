//
//  Rest.swift
//  Marcin362DevKit
//
//  Created by Marcin Ambrochowicz on 16.11.2016.
//  Copyright © 2016 Marcin Ambrochowicz. All rights reserved.
//

import Foundation

public class Rest {
    
    public init(baseUrl: URL) {
        self.baseUrl = baseUrl
    }
    
    public func execute<T: RestResponse>(restRequest: RestRequest, withResponseType: T.Type, completionHandler: @escaping (T) -> Void) {
        session.dataTask(with: urlRequest(fromRestRequest: restRequest)) { (data, urlResponse, error) in
            var statusCode = 0
            
            if let httpResponse = urlResponse as? HTTPURLResponse {
                statusCode = httpResponse.statusCode
            }
            
            if let error = error as? NSError {
                statusCode = error.code
            }
    
            let restResponse = T(code: statusCode, data: data)
            
            completionHandler(restResponse)
        }.resume()
    }
    
    // MARK: - Private
    
    private let session: URLSession = {
        let configuration = URLSessionConfiguration.default
        configuration.httpMaximumConnectionsPerHost = 1
        
        let session = URLSession(configuration: configuration)
        
        return session
    }()
    
    private let baseUrl: URL
    
    private func urlRequest(fromRestRequest restRequest: RestRequest) -> URLRequest {
        var urlString = baseUrl.appendingPathComponent(restRequest.endpoint).absoluteString
        if let paramsString = restRequest.paramsString {
            urlString.append("?")
            urlString.append(paramsString)
        }
        
        let url = URL(string: urlString)!
        
        var request = URLRequest(url: url)
        request.httpMethod = restRequest.method.rawValue
        for (key, value) in restRequest.headers ?? [:] {
            request.setValue(value, forHTTPHeaderField: key)
        }
        
        return request
    }
    
}
