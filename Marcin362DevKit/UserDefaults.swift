//
//  UserDefaults.swift
//  Marcin362DevKit
//
//  Created by Marcin Ambrochowicz on 03.10.2016.
//  Copyright © 2016 Marcin Ambrochowicz. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    public var username: String? {
        set {
            UserDefaults.standard.set(newValue, forKey: "Username")
        }
        get {
            return UserDefaults.standard.object(forKey: "Username") as? String
        }
    }
    
    public var firstName: String? {
        set {
            UserDefaults.standard.set(newValue, forKey: "FirstName")
        }
        get {
            return UserDefaults.standard.object(forKey: "FirstName") as? String
        }
    }
    
    public var lastName: String? {
        set {
            UserDefaults.standard.set(newValue, forKey: "LastName")
        }
        get {
            return UserDefaults.standard.object(forKey: "LastName") as? String
        }
    }
    
}
