//
//  NSManagedObject.swift
//  Marcin362DevKit
//
//  Created by Marcin Ambrochowicz on 19.11.2016.
//  Copyright © 2016 Marcin Ambrochowicz. All rights reserved.
//

import CoreData

extension NSManagedObject {
    
    open var isValid: Bool {
        if isInserted {
            do {
                try validateForInsert()
            } catch {
                return false
            }
        }
        
        if isUpdated {
            do {
                try validateForUpdate()
            } catch {
                return false
            }
        }
        
        if isDeleted {
            do {
                try validateForDelete()
            } catch {
                return false
            }
        }
        
        return true
    }

}
