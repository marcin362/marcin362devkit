//
//  DesignableView.swift
//  Marcin362DevKit
//
//  Created by Marcin Ambrochowicz on 01.11.2016.
//  Copyright © 2016 Marcin Ambrochowicz. All rights reserved.
//

import UIKit

@IBDesignable public class DesignableView : UIView {
    
    // MARK: - IB

    @IBInspectable public var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            gradientLayer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable public var gradientBackgroundColor: UIColor? = nil {
        didSet {
            gradientLayer.colors = [gradientBackgroundColor!.cgColor, backgroundColor?.cgColor ?? UIColor.clear.cgColor]
        }
    }
    @IBInspectable public var gradientBackgroundStartPoint: CGPoint = CGPoint(x: 0.0, y: 0.5) {
        didSet {
            gradientLayer.startPoint = gradientBackgroundStartPoint
        }
    }
    @IBInspectable public var gradientBackgroundEndPoint: CGPoint = CGPoint(x: 1.0, y: 0.5) {
        didSet {
            gradientLayer.endPoint = gradientBackgroundEndPoint
        }
    }
    
    // MARK: - UIView
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        gradientLayer.frame = CGRect(x: 0, y: 0, width: layer.frame.width, height: layer.frame.height)
    }
    
    // MARK: - Private
    
    private var gradientLayer: CAGradientLayer {
        for layer in layer.sublayers ?? [] {
            if layer.name == "Marcin362DevKitGradientLayer" {
                return layer as! CAGradientLayer
            }
        }
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.name = "Marcin362DevKitGradientLayer"
        gradientLayer.frame = CGRect(x: 0, y: 0, width: layer.frame.width, height: layer.frame.height)
        
        layer.insertSublayer(gradientLayer, at: 0)
        
        return gradientLayer
    }

}
