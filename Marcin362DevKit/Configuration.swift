//
//  Configuration.swift
//  Marcin362DevKit
//
//  Created by Marcin Ambrochowicz on 02.10.2016.
//  Copyright © 2016 Marcin Ambrochowicz. All rights reserved.
//

import Foundation

public enum ConfigurationError : Error {
    case plistNotExist
    case plistIncorrect
}

open class Configuration {

    public let data: [String : Any]
    
    public init(plistName: String = "Configuration") throws {
        guard let path = Bundle.main.path(forResource: plistName, ofType: "plist") else {
            throw ConfigurationError.plistNotExist
        }
        
        guard let dataDictionary = NSDictionary(contentsOfFile: path) else {
            throw ConfigurationError.plistIncorrect
        }
        
        data = dataDictionary as! [String : Any]
    }

}
