//
//  FetchedResultsControllerDelegate.swift
//  Marcin362DevKit
//
//  Created by Marcin Ambrochowicz on 12.11.2016.
//  Copyright © 2016 Marcin Ambrochowicz. All rights reserved.
//

import Foundation
import CoreData

public class FetchedResultsControllerDelegate : NSObject, NSFetchedResultsControllerDelegate {
    
    public init(tableView: UITableView) {
        self.tableView = tableView
    }
    
    // MARK: - NSFetchedResultsControllerDelegate
    
    public func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView?.beginUpdates()
    }
    
    public func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView?.insertRows(at: [newIndexPath!], with: .automatic)
        case .update:
            tableView?.reloadRows(at: [indexPath!], with: .automatic)
        case .move:
            tableView?.deleteRows(at: [indexPath!], with: .automatic)
            tableView?.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            tableView?.deleteRows(at: [indexPath!], with: .automatic)
        }
    }
    
    public func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView?.endUpdates()
    }
    
    // MARK: - Private
    
    private weak var tableView: UITableView?
    
}
