//
//  DesignableButton.swift
//  Marcin362DevKit
//
//  Created by Marcin Ambrochowicz on 15.11.2016.
//  Copyright © 2016 Marcin Ambrochowicz. All rights reserved.
//

import UIKit

@IBDesignable public class DesignableButton : UIButton {
    
    // MARK: - IB
    
    @IBInspectable public var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable public var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    @IBInspectable public var borderWidth: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
}
